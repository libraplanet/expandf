var shell = new ActiveXObject('WScript.Shell');
var fso = new ActiveXObject('Scripting.FileSystemObject');

/**
 * implement trim on String.
 * https://blog.halt.project-p.jp/2006/01/20/195213/
 */
String.prototype.trim = function(){ return this.replace(/(^\s+|\s+$)/g, '') };


/**
 * echo.
 * @param str
 */
var echo = function(str) {
  //WScript.StdOut.WriteLine(str);
  //WScript.StdOut.Write(str + '\n');
  WScript.Echo(str);
}

/**
 * isCScript.
 * @return boolean
 */
var isCScript = function() {
  var interpreter = fso.GetFileName(WScript.FullName);
  return (interpreter.toLowerCase() == 'cscript.exe');
}

/**
 * getArgList.
 * @return Array from WScript.Arguments.
 */
var getArgList = function() {
  var args = WScript.Arguments;
  var ret = [];
  for(var i = 0; i < args.length; i++) {
    ret.push(args(i));
  }
  return ret;
}

/**
 * getStdInLineList.
 * @return string list from WScript.StdIn on lines.
 */
var getStdInLineList = function() {
  var eof = String.fromCharCode(0x1A);
  var ret = [];
  /**
   * JScript StdIn.StdOut
   * https://ooo.iiyudana.net/htm/js_chp27frame.htm
   */
  while (!WScript.StdIn.AtEndOfStream){
    var str = WScript.StdIn.ReadLine();
    var index = str.indexOf(eof);
    if(index < 0) {
      ret.push(str);
    } else if(index > 0) {
      ret.push(str.substring(0, index));
    }
    if(index >= 0) {
      break;
    }
  }
  return ret;
}

/**
 * echo help.
 * @param addLines
 */
var echoHelp = function(addLines) {
  var lines = [];
  lines.push('Usage: cscript.exe /nologo expandf.js [Options]');
  lines.push('expand path list on standard input to file path list.');
  lines.push('*support system charset only (WSH\'s spec).');
  lines.push('');
  lines.push('Options:');
  lines.push('   -dfl,  --dummy-first-line   skip first line (you use, when you added dummy line on pipe on first).');
  lines.push('   -dll,  --dummy-last-line    skip last line (you use, when you added dummy line on pipe on last).');
  lines.push('   -full, --output-full-path   output for full path (default is argument path base).');
  lines.push('   -/,    --force-slash        force replace path sepalator to slash (unix path like).');
  lines.push('   -\\,    --force-backslash   force replace path sepalator to backslash (windows path like).');
  lines.push('   -lf,   --force-newline-lf   force replace path sepalator to slash (unix path like).');
  lines.push('   -crlf, --force-newline-crlf force replace path sepalator to backslash (windows path like).');
  lines.push('');
  lines.push('Example:');
  lines.push('   echo %CD% | cscript.exe /nologo expandf.js');
  lines.push('   echo . | cscript.exe /nologo expandf.js -/ -lf');
  lines.push('   type list.txt | cscript.exe /nologo expandf.js -\\');
  lines.push('   type list.txt | cscript.exe /nologo expandf.js -/ -full -lf');

  if(addLines) {
    lines.push('');
    lines = lines.concat(addLines);
  }
  echo(lines.join('\n'));
}

/**
 * eachCollection.
 * @param collection
 * @param callback
 */
var eachCollection = function(collection, callback) {
  if(callback) {
    var enumerator = new Enumerator(collection);
    while(!enumerator.atEnd()) {
      callback(enumerator.item());
      enumerator.moveNext();
    }
  }
};


/**
 * startsWith.
 * @param str0
 * @param string list from 
 * @return boolean
 */
var startsWith = function(str0, str1) {
  if((!str0) || (!str1)) {
    return false;
  } else if(str0.length < str1.length) {
    return false;
  } else {
    return (str0.indexOf(str1) == 0);
  }
}

/**
 * startsWith.
 * @param str0
 * @param str1
 * @return boolean
 */
var endsWith = function(str0, str1) {
  if((!str0) || (!str1)) {
    return false;
  } else if(str0.length < str1.length) {
    return false;
  } else {
    return (str0.indexOf(str1) == (str0.length - str1.length));
  }
}

/**
 * get options from arguments.
 * @param arguments by string list.
 * @return options by hash list.
 */
var getOptions = function(args) {
  var opts = {
    isHelp: false,
    isDummyFirstLine: false,
    isDummyLastLine:false,
    isOutputFullPath:false,
    forceDirectorySeparator: null,
    forceNewline: null,
    addHelpLines: null
  };
  if(args) {
    for(var i = 0; i < args.length; i++) {
      var arg = args[i];
      if(startsWith(arg, '-')) {
        switch(arg) {
          case '-h':
          case '--help':
            opts.isHelp = true;
            break;
          case '-dfl':
          case '--dummy-first-line':
            opts.isDummyFirstLine = true;
            break;
          case '-dll':
          case '--dummy-last-line':
            opts.isDummyLastLine = true;
            break;
          case '-full':
          case '--output-full-path':
            opts.isOutputFullPath = true;
            break;
          case '-/':
          case '--force-slash':
            opts.forceDirectorySeparator = '/';
            break;
          case '-\\':
          case '--force-backslash':
            opts.forceDirectorySeparator = '\\';
            break;
          case '-lf':
          case '--force-newline-lf':
            opts.forceNewline = '\n';
            break;
          case '-crlf':
          case '--force-newline-crlf':
            opts.forceNewline = '\r\n';
            break;
          default :
            opts.isHelp = true;
            if(!opts.addHelpLines) {
              opts.addHelpLines = [];
            }
            opts.addHelpLines.push('illegal agument "' + arg + '"');
            break;
        }
      }
    }
  }
  return opts;
}

/**
 * @param lines
 * @param opt
 * @param writeLine callback
 */
var scan = function(lines, opts, writeLine) {
  // list
  for(var i = 0; i < lines.length; i++) {
    var line = lines[i];
    var searchPath = line.trim();
    if((searchPath == '') || startsWith(searchPath, '#')) {
      writeLine(line);
    } else {
      if(fso.FileExists(searchPath)) {
        // it is file.
        var path = searchPath;
        if(opts.forceDirectorySeparator) {
          path = path.replace(new RegExp('\\\\', 'g'), opts.forceDirectorySeparator);
          path = path.replace(new RegExp('/', 'g'), opts.forceDirectorySeparator);
        }
        writeLine(path);
      } else if(fso.FolderExists(searchPath)){
        // it is directory.
        var rootSrcPath = searchPath;
        var rootObj = fso.GetFolder(rootSrcPath);
        var dig = function(objFolder) {
          eachCollection(objFolder.subfolders, function(objSubFolder) {
            dig(objSubFolder);
          });
          eachCollection(objFolder.files, function(file) {
            var path = file.Path;

            if(!opts.isOutputFullPath) {
              var rootFullPath = rootObj.Path;
              var rootArgPath = rootSrcPath;
              path = fso.BuildPath(rootArgPath, path.replace(rootFullPath, ''));
            }
            if(opts.forceDirectorySeparator) {
              path = path.replace(new RegExp('\\\\', 'g'), opts.forceDirectorySeparator);
              path = path.replace(new RegExp('/', 'g'), opts.forceDirectorySeparator);
            }
            writeLine(path);
          });
        };
        dig(rootObj);
      } else {
        // unknown.
        var mes = '# not found!!!';
        mes += ' fileExits(' + fso.FileExists(searchPath) + ')';
        mes += ' folderExists(' + fso.FolderExists(searchPath) + ')';
        mes += ' ' + line;
        writeLine(mes);
      }
    }
  }
}

/**
 * main.
 * @param args
 */
var main = function(args) {
  if(!isCScript()) {
    echoHelp([
      '"' + fso.GetFileName(WScript.FullName) + '" is not cscript.',
      'please use cscript.'
    ]);
  } else {
    var opts = getOptions(args);
    if(opts.isHelp) {
      echoHelp(opts.addHelpLines);
    } else {
      // get from SrdIn
      var lines = getStdInLineList();
      var writeLine = echo;

      if(opts.isDummyFirstLine) {
        lines.shift();
      }
      if(opts.isDummyLastLine) {
        lines.pop();
      }

      // overwrite output function.
      if(opts.forceNewline) {
        writeLine = function(str) {
          WScript.StdOut.Write(str + opts.forceNewline);
        }
      }

      // scan
      scan(lines, opts, writeLine);
    }
  }
}

// entry point.
main(getArgList());
