# EXPAND Fils
## 概要
フォルダを捜査しファイルの一覧に展開するツール。  
複数のフォルダ情報を一括展開する。  
展開時にディレクトリ セパレーターの置き換え指定可能。

## 動作仕様
* オプション：コマンドの引数で指定
* 捜査対象のフォルダ リストの入力：標準入力
* ファイル リストの出力：標準出力

## 開発環境
* Windows 10
* Visual Studio 20019

## 動作環境
### WSH版
* Windows 
* システム文字コード  ※ 制約多め

### C#版
* Windows 
* .NET Framework 4.6
* UTF-8

### オプション
```
Usage: expandf [Options]
expand path list on standard input to file path list.

Options:
   -dfl,  --dummy-first-line   skip first line (you use, when you added dummy line on pipe on first).
   -dll,  --dummy-last-line    skip last line (you use, when you added dummy line on pipe on last).
   -full, --output-full-path   output for full path (default is argument path base).
   -/,    --force-slash        force replace path sepalator to slash (unix path like).
   -\,    --force-backslash    force replace path sepalator to backslash (windows path like).
   -lf,   --force-newline-lf   force replace path sepalator to slash (unix path like).
   -crlf, --force-newline-crlf force replace path sepalator to backslash (windows path like).

Example:
   echo %CD% | expandf
   echo . | expandf -/ -lf
   type list.txt | expandf -\
   type list.txt | expandf -/ -full -lf
```


## 動作例
以下の様なファイル構成に対しての実行例  
![sample - fils.png](https://bitbucket.org/libraplanet/expandf/raw/0469cf180499feef8dc878d65374a4f5bd7f5a98/images/sample%20-%20fils.png)

### (例) コマンド
* コマンド実行例  
![sample - command (1).png](https://bitbucket.org/libraplanet/expandf/raw/0469cf180499feef8dc878d65374a4f5bd7f5a98/images/sample%20-%20command%20%281%29.png)

### (例) EmEditor
* 外部ツール設定  
![settings - emeditor (cs).png](https://bitbucket.org/libraplanet/expandf/raw/0469cf180499feef8dc878d65374a4f5bd7f5a98/images/settings%20-%20emeditor%20%28cs%29.png)

* 入力テキスト  
![sample - input.png](https://bitbucket.org/libraplanet/expandf/raw/0469cf180499feef8dc878d65374a4f5bd7f5a98/images/sample%20-%20input.png)

* 出力テキスト  
![sample - output.png](https://bitbucket.org/libraplanet/expandf/raw/0469cf180499feef8dc878d65374a4f5bd7f5a98/images/sample%20-%20output.png)  
UTF-8での処理のため、国際文字、サロゲートペア、UNICODE絵文字も一応OK

## 制限事項
### WSH版
* システム文字コードにまつわる諸々  
　※以下、日本語環境の場合
    * サロゲート文字の使用不可  
      ( ``𩸽``、``𠮷``、など)
    * 日本語以外の国際文字の使用不可
    * UNICODE絵文字の使用不可
    * UNICODE絵文字固有文字の使用不可  
      (WAVE DASH問題に於ける、``〜`` (``U+301C``))

