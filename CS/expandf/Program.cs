﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace Expandf
{
    class Program
    {
        class Options
        {
            public bool isHelp = false;
            public bool isDummyFirstLine = false;
            public bool isDummyLastLine = false;
            public bool isOutputFullPath = false;
            public string forceDirectorySeparator = null;
            public string forceNewline = null;
            public List<String> addHelpLines = null;
        }

        static void Echo(string str)
        {
            Console.WriteLine(str);
        }

        static List<string> GetStdInLineList ()
        {
            char eof = (char)0x1A;
            List<string> ret = new List<string>();
            string str;


            while((str = Console.ReadLine()) != null)
            {
                var index = str.IndexOf(eof);
                if (index < 0)
                {
                    ret.Add(str);
                }
                else if (index > 0)
                {
                    ret.Add(str.Substring(0, index));
                }
                if (index >= 0)
                {
                    break;
                }
            }
            return ret;
        }

        static void EchoHelp(List<String> addLines)
        {
            List<String> lines = new List<String>();
            lines.Add(@"expandf v0.0.0.1");
            lines.Add(@"author: takumi (Jul. 27th, 2020)");
            lines.Add(@"*support utf-8 only.");
            lines.Add(@"");
            lines.Add(@"Usage: expandf [Options]");
            lines.Add(@"expand path list on standard input to file path list.");
            lines.Add(@"");
            lines.Add(@"Options:");
            lines.Add(@"   -dfl,  --dummy-first-line   skip first line (you use, when you added dummy line on pipe on first).");
            lines.Add(@"   -dll,  --dummy-last-line    skip last line (you use, when you added dummy line on pipe on last).");
            lines.Add(@"   -full, --output-full-path   output for full path (default is argument path base).");
            lines.Add(@"   -/,    --force-slash        force replace path sepalator to slash (unix path like).");
            lines.Add(@"   -\,    --force-backslash    force replace path sepalator to backslash (windows path like).");
            lines.Add(@"   -lf,   --force-newline-lf   force replace path sepalator to slash (unix path like).");
            lines.Add(@"   -crlf, --force-newline-crlf force replace path sepalator to backslash (windows path like).");
            lines.Add(@"");
            lines.Add(@"Example:");
            lines.Add(@"   echo %CD% | expandf");
            lines.Add(@"   echo . | expandf -/ -lf");
            lines.Add(@"   type list.txt | expandf -\");
            lines.Add(@"   type list.txt | expandf -/ -full -lf");

            if (addLines != null)
            {
                lines.Add("");
                lines.AddRange(addLines);
            }

            Echo(string.Join("\n", lines));
        }

        static Options GetOptions(string[] args)
        {
            Options opts = new Options();
            if (args != null)
            {
                for (var i = 0; i < args.Length; i++)
                {
                    string arg = args[i];
                    if (arg.StartsWith("-"))
                    {
                        switch (arg)
                        {
                            case "-h":
                            case "--help":
                                opts.isHelp = true;
                                break;
                            case "-dfl":
                            case "--dummy-first-line":
                                opts.isDummyFirstLine = true;
                                break;
                            case "-dll":
                            case "--dummy-last-line":
                                opts.isDummyLastLine = true;
                                break;
                            case "-full":
                            case "--output-full-path":
                                opts.isOutputFullPath = true;
                                break;
                            case "-/":
                            case "--force-slash":
                                opts.forceDirectorySeparator = "/";
                                break;
                            case "-\\":
                            case "--force-backslash":
                                opts.forceDirectorySeparator = "\\";
                                break;
                            case "-lf":
                            case "--force-newline-lf":
                                opts.forceNewline = "\n";
                                break;
                            case "-crlf":
                            case "--force-newline-crlf":
                                opts.forceNewline = "\r\n";
                                break;
                            default:
                                opts.isHelp = true;
                                if (opts.addHelpLines == null)
                                {
                                    opts.addHelpLines = new List<String>();
                                }
                                opts.addHelpLines.Add(string.Format("illegal agument \"{0}\"", arg));
                                break;
                        }
                    }
                }
            }
            return opts;
        }

        static void scan(List<string> lines, Options opts, Action<string> writeLine)
        {
            // list
            for (int i = 0; i < lines.Count; i++)
            {
                string line = lines[i];
                string searchPath = line.Trim();
                if (string.IsNullOrEmpty(searchPath) || searchPath.StartsWith("#"))
                {
                    writeLine(line);
                }
                else
                {
                    if (File.Exists(searchPath))
                    {
                        // it is file.
                        string path = searchPath;
                        if (opts.forceDirectorySeparator != null)
                        {
                            path = Regex.Replace(path, @"[\\/]", opts.forceDirectorySeparator);
                        }
                        writeLine(path);
                    }
                    else if (Directory.Exists(searchPath))
                    {
                        // it is directory.
                        string rootSrcPath = searchPath;
                        Action<string> dig = null;
                        dig = delegate (string folderOath) {
                            try
                            {
                                foreach (string subfolderPath in Directory.GetDirectories(folderOath))
                                {
                                    dig(subfolderPath);
                                }
                                foreach (string filePath in Directory.GetFiles(folderOath))
                                {
                                    string path = filePath;
                                    if (opts.isOutputFullPath)
                                    {
                                        path = Path.GetFullPath(filePath);
                                    }
                                    if (opts.forceDirectorySeparator != null)
                                    {
                                        path = Regex.Replace(path, @"[\\/]", opts.forceDirectorySeparator);
                                    }
                                    writeLine(path);
                                }
                            }
                            catch (Exception e)
                            {
                                writeLine("# appear exception!!!" + " " + folderOath);
                                writeLine("# " + e.Message);
                            }
                        };
                        dig(rootSrcPath);
                    } else {
                        // unknown.
                        string mes = "# not found!!!";
                        mes += " fileExits(" + File.Exists(searchPath) + ")";
                        mes += " folderExists(" + Directory.Exists(searchPath) + ")";
                        mes += " " + line;
                        writeLine(mes);
                    }
                }
            }
        }

        static void Main(string[] args)
        {
            Options opts;

            Console.InputEncoding = Encoding.UTF8;
            Console.OutputEncoding = Encoding.UTF8;

            opts = GetOptions(args);

            if (opts.isHelp)
            {
                EchoHelp(opts.addHelpLines);
            }
            else
            {
                // get from SrdIn
#if DEBUG
#if TRUE
                List<string> lines = GetStdInLineList();
#else
                List<string> lines = new List<string>();
                lines.Add(@"C:\Windows\System32\drivers");
                //lines.Add(@"../");
                //lines.Add(@"../../bin");
                //lines.Add(@"../../");
#endif
#else
                List<string> lines = GetStdInLineList();
#endif
                Action<string> writeLine = Echo;

                if (opts.isDummyFirstLine)
                {
                    lines.RemoveAt(0);
                }
                if (opts.isDummyLastLine)
                {
                    lines.RemoveAt(lines.Count - 1);
                }

                // overwrite output function.
                if (opts.forceNewline != null)
                {
                    writeLine = delegate (string str)
                    {
                        Console.Write(str + opts.forceNewline);
                    };
                }

                // scan
                scan(lines, opts, writeLine);
            }
        }
    }
}
